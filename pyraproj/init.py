from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.response import Response
from pyramid.view import view_config
from info import EmpClass as c
from helloworld import HelloController as h

def hello_world(request):
    return Response('Hello %(name)s!' % request.matchdict)

def contact_us(request):
    c.title = "hello1"
    return {"c":c}
        
if __name__=='__main__':
    with Configurator() as config:
        config.include('pyramid_mako')

        config.add_route('hello','/hello/{name}')
        config.add_view(hello_world,route_name='hello')

        config.add_route('test','/test')
        config.add_view(contact_us,route_name='test',renderer='templates/test.mako')
        
        config.add_route('test1','/test1')
        config.add_view(h.test1,route_name='test1',renderer='templates/test1.mako')

        app = config.make_wsgi_app()
    server = make_server('0.0.0.0',8080,app)
    server.serve_forever()