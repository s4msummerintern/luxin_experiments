from wsgiref.simple_server import make_server
from pyramid.config import Configurator
from pyramid.response import Response
from pyramid.view import view_config
from info import EmpClass as c

class BaseController:
    
    c.title = 'title' 
    def hello_world(request):
        return Response('Hello %(name)s!' % request.matchdict)

